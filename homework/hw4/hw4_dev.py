"""template code for hw4
"""
import numpy as np
import matplotlib.pyplot as plt
from ns import netstats as ns #assumes netstats module has been compiled as ns.so with f2py


def convergence(n0,l,nt,m,numthreads,display=False):
    """test convergence of qmax_ave"""
	
    #define a vector mvalues that contains all the values of m in ascending order
    mvalues=np.arange(m)+1
	
    #define qmaxA to be a vector of zeros - it will store the max value (qmax) across m networks for each m
    qmax_qnetm=np.zeros(m)
	
    #define qmax_ave to be a vector of zeros - it will store the average value of qmax across m networks
    qmax_ave=np.zeros(m)
	
    #call subroutine stats from fortran module netstats, and assign the output values to qnetm,qmaxm,qvarm
    qnetm,qmaxm,qvarm=ns.stats(n0,l,nt,m)
        
        
    for i in range(0,m):
            
        for j in range(0,m):
            #assign the max value of j-th column of qnetm to qmax_qnetm
            qmax_qnetm[j]=np.max(qnetm[:,j]) 
            
        #find the average of first i+1 qmax_qnetm values and assign to qmax_ave   
        qmax_ave[i]=np.sum(qmax_qnetm[0:i+1])/(i+1)
            
    #use np.polyfit to estimate the rate of convergence of qmax_ave (we will use power law form: P ~ qmax_ave^k)
    k1,l = np.polyfit(np.log(mvalues),np.log(qmax_ave),1)
    
    #set k to be -k1 since we want to use the power law form: P ~ qmax_ave^(-k)
    k=-k1
    
    print k
    
    #check if the display is equal to True
    if display==True:
            
        #create a plot
        plt.figure()
            
        #create a log-log plot qmax_ave vs values of m
        plt.loglog(mvalues,qmax_ave, label='actual plot')
        
        #plot the fit to the log-log plot
        plt.loglog(mvalues,np.exp(l)*mvalues**(-k),'k--', label='fit to the plot, k=%f' %(k))
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: convergence, m=%d' %(m))
        plt.xlabel('m')
        plt.ylabel('qmax_ave')
        plt.legend()
        
        #plot the grid
        plt.grid()
    
        #show the plot
        plt.show() 

    #return k
    return k
	
def speedup(n0,l,ntarray,marray):
    """test speedup of stats_par"""
    
    """I have provided three figures to better analyse how the various values of nt and m affect
    the speedup. The first figure hw421.png shows 4 graphs, each plotted for the values of m
    varrying in the range between 1 and 200, and for a fixed nt: 5,50,100,200 respectively. From the graph
    one can notice the trend: for small values of nt, e.g nt=5, the parallelization is not efficient. The 
    speed up factor (time before the code was parallelized over the time after the code was parallelized)
    varries in the range (0,0.5). However, for the large values of nt, e.g. 50,100,200, the parallelization 
    shows a significant result. For all these graphs, the larger is the m, the better is the parallelization, which
    is specifically clear for m in the interval (1,50), where the increasing trend is particarly noticeable. 
    Afterwards, as m keeps on increasing, the speed up factor does not change significantly and converges to 2. 
    The convergence is better seen for the largest value of nt, nt=200. The reason it converges to the value of 2, 
    is because I am parallelizing the code using 2 cores.
    
    The second figure hw422.png shows 2 graphs, each plotted for the values of nt varrying in the range
    between 1 and 200, and for a fixed m: 10 and 100 respectively. I have chosen a large and a small value of m.
    Each graph, as before, is plotted for the number of threads eqaul to 2. From the figure,we can see that, when
    m is small, m=10, the parallelization in inneficient for the values of nt up to approximately 50. Afterwards, the 
    speed up factor starts slowly increasing, and becomes efficient for significantly large values of nt. 
    However, when m is large, m=100, the parallelized code faster leads to an increase in efficiency: after 
    approximately nt=25, the speed up factor becomes greater than 1. For both of the graphs, the speed up factor 
    converges to 2 as the value of nt becomes significantly large. To plot this figure, I have chosen only 2 values
    of m, 10 and 100, to analyze because when choosing more or larger values of m, my code was taking too long to run.
    
    The third figure hw423.png shows 3 graphs, each plotted for the values of m varrying in the range
    between 1 and 200, and for a fixed nt=200. I have chosen the large value of nt to be able to see
    the noticeable trends in the speed up time changes (from before we have seen that for small values
    of nt, the parallelization is inefficient). I have plotted each graph for a different number of threads,
    that were used to parallelize the code. It is clearly seen that, the more cores are used in the code's
    parallelization, the more efficient the parallelization becomes: the speedup factor converges to 2 when
    using 2 cores, however it converges to 4 when using 4 cores as the value of m increases. Also, the larger is
    the m value, the better the parallelization becomes: speed up factor increases significantly."""
    
	
    #define walltime to be a matrix of zeros with dimensions of length of ntarray and of marray
    walltime_cores12=np.zeros((len(ntarray),len(marray)))
    walltime_cores13=np.zeros((len(ntarray),len(marray)))
    walltime_cores14=np.zeros((len(ntarray),len(marray)))
    
    for i in range(0,len(ntarray)):
        for j in range(0,len(marray)):
        
            #assign walltime values from non-paralellized and parallelized codes to walltime1 and walltime2,3,4 respectively
            walltime1=ns.test_stats_omp(n0,l,ntarray[i],marray[j],1)
            walltime2=ns.test_stats_omp(n0,l,ntarray[i],marray[j],2)
            walltime3=ns.test_stats_omp(n0,l,ntarray[i],marray[j],3)
            walltime4=ns.test_stats_omp(n0,l,ntarray[i],marray[j],4)
    
            #find the speedup ratio, assign to walltime
            walltime_cores12[i,j]=walltime1/walltime2
            walltime_cores13[i,j]=walltime1/walltime3
            walltime_cores14[i,j]=walltime1/walltime4
            

    threads_const_m_varries = False
    threads_const_nt_varries = True
    threads_change = False
    
    if threads_const_m_varries == True:
        
        #create a plot
        plt.figure()
            
        #create plots for all values of ntarray and marray, for each graph nt is fixed
        #and m varries in the range of marray
        for i in range(0,len(ntarray)):
            plt.plot(marray,walltime_cores12[i,:], label='Nt=%d' %(ntarray[i]))
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: speedup, numthreads = 2')
        plt.xlabel('m')
        plt.ylabel('Walltime speedup')
        plt.legend()
            
        #plot the grid
        plt.grid()
    
        #show the plot
        plt.show() 
        
    
    if threads_const_nt_varries == True:
        
        #create a plot
        plt.figure()
            
        #create plots for all values of ntarray and marray, for each graph m is fixed
        #and nt varries in the range of ntarray
        for i in range(0,len(marray)):
            plt.plot(ntarray,walltime_cores12[:,i], label='m=%d' %(marray[i]))
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: speedup, numthreads = 2')
        plt.xlabel('nt')
        plt.ylabel('Walltime speedup')
        plt.legend()
            
        #plot the grid
        plt.grid()
    
        #show the plot
        plt.show() 
        
    
    if threads_change == True:
        
        #create a plot
        plt.figure()
            
        #create plots for all values of ntarray and marray, for each graph nt is fixed
        #and m varries in the range of marray, value of threads change from 2 to 4
        plt.plot(marray,walltime_cores12[i,:], label='Threads=2' )
        plt.plot(marray,walltime_cores13[i,:], label='Threads=3' )
        plt.plot(marray,walltime_cores14[i,:], label='Threads=4' )
        
        #add title, legend and axis labels
        i=0
        plt.title('User: Anna Mitenkova, function: speedup, nt = %d' %(ntarray[i]))
        plt.xlabel('m')
        plt.ylabel('Walltime speedup')
        plt.legend()
            
        #plot the grid
        plt.grid()
    
        #show the plot
        plt.show() 

def degree_average(n0,l,nt,m,display=False):
    """compute ensemble-averaged degree distribution"""	
    
    #call subroutine stats from fortran module netstats, and assign the output values to qnetm,qmaxm,qvarm
    qnetm,qmaxm,qvarm=ns.stats(n0,l,nt,m)
    
    #define P to be the matrix of zeros with dimensions n0+nt and m
    P=np.zeros((n0+nt,m))
    
    #define P_ave to be the vector of zeros with dimensions n0+nt
    P_ave=np.zeros(n0+nt)
    
    #define x to be the vector of consequtive integers from 1 to n0+nt
    x=np.arange(n0+nt)+1
    
    #define bins to be the vector of zeros with dimension n0+nt
    bins=np.zeros(n0+nt)

    #compute the distribution P(d)
    for i in range(0,m):
        bins = np.bincount(qnetm[:,i])
        P[0:len(bins),i] = (1.0*bins)/(n0+nt)
     
    #find the average of the distribution P, and store it as P_ave   
    for i in range(0,n0+nt):
        P_ave[i]=np.mean(P[i,:])
     
    #define d to be the vector of zeros
    d=np.zeros((len(P_ave[P_ave>0])))
    
    #set k to be zero
    k=0
    
    #set d to contain the values of x, corresponding to the values of P_ave
    for i in range(0,len(P_ave)):
        if P_ave[i]>0:
            d[k]=x[i]
            k=k+1

    #delete all the values that are zeros from P_ave
    P_ave=P_ave[P_ave>0]
    
    #use np.polyfit to estimate the rate of convergence of qmax_ave (we will use power law form: P ~ qmax_ave^k)
    k1,l = np.polyfit(np.log(d),np.log(P_ave),1)
    
    #set k to be -k1 since we want to use the power law form: P ~ qmax_ave^(-k)
    k=-k1
    
    print k
    
    #check if the display is equal to True
    if display==True:
            
        #create a plot
        plt.figure()
            
        #create a log-log plot P_ave vs d
        plt.loglog(d,P_ave,'o', label='average of %d distributions' %m )
        
        #plot the fit to the log-log plot
        plt.loglog(d,np.exp(l)*d**(-k),'k--', label='fit to the average distribution')
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: degree_average')
        plt.xlabel('d')
        plt.ylabel('P_ave')
        plt.legend()
        
        #plot the grid
        plt.grid()
    
        #show the plot
        plt.show()
    
    return -k
    
#use main to run the functions
if __name__ == "__main__":
    Convergence = False
    Speedup = False
    Degree_average = False
    
    #run the function convergence for the values n0=5, l=2, nt=400, m=1000, display=True    
    if Convergence == True:
        convergence(5,2,400,1000,1,True)
        
    #run the function speedup:
    if Speedup == True:
        
        """I used the call for the speedup function below, to test how various values of 
        nt and m affect the performance. I plot 4 graphs for each fixed value of nt.
        To run it, change threads_const_m_varries in the code to True"""
        speedup(5,2,[5,50,100,200],np.arange(200)+1)
        
        """I used the call for the speedup function below, to test how various values of 
        nt and m affect the performance. I plot 2 graphs for each fixed value of m.
        To run it, change threads_const_nt_varries in the code to True"""
        #speedup(5,2,np.arange(200)+1,[10,100])
        
        """I used the call for the speedup function below, to test how various values of 
        threads and m affect the performance. I have plotting the graphs for nt=200, it takes a
        few minutes to run, however it gives a very clear pictures of the treands. 
        To run it, change threads_change in the code to True """
        #speedup(5,2,[200],np.arange(200)+1)
     
    #run the function degree_average for the values n0=5, l=2, nt=400, m=1000, display=True    
    if Degree_average == True:
        degree_average(5,2,400,1000,True)
        

