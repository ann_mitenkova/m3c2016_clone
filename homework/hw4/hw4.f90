!Anna Mitenkova
!CID: 00927771

!Should be compiled with network.f90

module netstats
	use network
	use omp_lib

	contains

subroutine stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
	!Input: 
	!n0,nl,nt: recursive network model parameters
	!m: number of network realizations to compute
	!Output:
	!qnetm: node lists for all m networks
	!qvarm: ensemble averages of var(q)
	!qmaxm: maximum qmax from m network realizations
	
	!all variables in code must be declared
	implicit none
	
	!define the type of the variables
	integer, intent(in) :: n0,l,nt,m
	integer, dimension(n0+nt,m), intent(out) :: qnetm
	integer, intent(out) :: qmaxm
	real(kind=8), intent(out) :: qvarm
	integer :: i1,qmax, ntotal
	real(kind=8) :: mean, var
	integer, dimension(n0+nt) :: qnet
	integer, dimension(n0+l*nt,2) :: enet

        !assign the values to ntotal,qmaxm,var,qvarm
	ntotal = n0+nt
	qmaxm=0
	var=0.d0
	qvarm=0.d0
	
	do i1=1,m
	
	   !define mean to be zero
	   mean=0.d0
	   
	   !call the subroutine generate from the module network
	   call generate(n0,l,nt,qmax,qnet,enet)
	   
	   !assign qnet to the first column of qnetm
	   qnetm(:,i1)=qnet
	   
	   !find the maximum element btween qmax and qmaxm, assign it to qmax
	   qmaxm=max(qmax,qmaxm)
	   
	   !find the mean value
	   mean=sum(qnet)/ntotal
	   
	   !calculate the dot product of qnet - mean vectors to find the variance 
	   var=dot_product((qnet-mean),(qnet-mean))/ntotal
	   
	   !assign new value to qvarm
	   qvarm=qvarm+var/m

        end do
        

end subroutine stats


subroutine stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)
	!Input: 
	!n0,nl,nt: recursive network model parameters
	!m: number of network realizations to compute
	!numThreads: number of threads for parallel computation
	!Output:
	!qnetm: node lists for all m networks
	!qvarm: ensemble averages of var(q)
	!qmaxm: maximum qmax from m network realizations
	
	
	!all variables in code must be declared
	implicit none
	
	!define the type of the variables
	integer, intent(in) :: n0,l,nt,m,numThreads
	integer, dimension(n0+nt,m), intent(out) :: qnetm
	integer, intent(out) :: qmaxm
	real(kind=8), intent(out) :: qvarm
	integer :: i1,qmax, ntotal, threadID
	real(kind=8) :: mean,sumvar,var
	integer, dimension(n0+nt) :: qnet
	integer, dimension(n0+l*nt,2) :: enet
	
	!assign the values to ntotal,qmaxm,var,qvarm
	ntotal = n0+nt
	qmaxm=0
	var=0.d0
	qvarm=0.d0
	
	call OMP_SET_NUM_THREADS(numThreads)
	
	!use OMP tp parallelize the code
	!$OMP parallel do private(mean,var,qnetm,qmax,qnet,enet,threadID), reduction(+:qvarm), reduction(max:qmaxm)
	do i1=1,m
	
	   !define mean to be zero
	   mean=0.d0
	   
	   !call the subroutine generate from the module network
	   call generate(n0,l,nt,qmax,qnet,enet)
	   
	   !assign qnet to the first column of qnetm
	   qnetm(:,i1)=qnet
	   
	   !find the maximum element btween qmax and qmaxm, assign it to qmax
	   qmaxm=max(qmax,qmaxm)
	   
	   !find the mean value
	   mean=sum(qnet)/ntotal
	   
	   !calculate the dot product of qnet - mean vectors to find the variance 
	   var=dot_product((qnet-mean),(qnet-mean))/ntotal
	   
	   !assign new value to qvarm
	   qvarm=qvarm+var/m
	   
	   !assign omp_get_thread_num() to threadID
	   threadID=omp_get_thread_num()

        end do
        !$OMP end parallel do

end subroutine stats_omp


subroutine test_stats_omp(n0,l,nt,m,numThreads,walltime)
	!Input: same as stats_omp
	!Output: walltime: time for 100 cals to stats_par
	
	!all variables in code must be declared
	implicit none
	
	!define the type of the variables
	integer, intent(in) :: n0,l,nt,m,numThreads
	real(kind=8), intent(out) :: walltime
	integer(kind=8) :: clock_t1,clock_t2,clock_rate
	real(kind=8) :: qvarm
	integer, dimension(n0+nt,m) :: qnetm
	integer :: qmaxm,i1
	
	!assign the values to walltime, clock_t1, cloack_t2
	walltime=0.d0
	clock_t1=0.d0
	clock_t2=0.d0
	
	!if the code is non-parallelized:
	if (numThreads==1) then
	   
	   do i1=1,100
	   
	       !start timimg
	       call system_clock(clock_t1)
	       
	       !call the subroutine stats from the module netstats
	       call stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
	       
	       !stop timing
	       call system_clock(clock_t2,clock_rate)
	       
	       !assign the time value to the walltime variable
	       walltime=walltime+dble(clock_t2-clock_t1)/dble(clock_rate)
	   end do
	   
	   !print the time
	   print *, 'average elapsed wall time (seconds) for non-parallelized code= ', walltime/100
	 
	!if the code is parallelized:  
	else if (numThreads>1) then
	
	   do i1=1,100
	   
	       !start timimg
	       call system_clock(clock_t1)
	       
	       !call the subroutine stats from the module netstats
	       call stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)
	       
	       !stop timing
	       call system_clock(clock_t2,clock_rate)
	       
	       !assign the time value to the walltime variable
	       walltime=walltime+dble(clock_t2-clock_t1)/dble(clock_rate)
	   end do
	   
	   !print the time
	   print *, 'average elapsed wall time (seconds) for parallelized code= ', walltime/100
	   
	end if



end subroutine test_stats_omp


end module netstats