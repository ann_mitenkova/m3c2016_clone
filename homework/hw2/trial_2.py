import numpy as np
import matplotlib.pyplot as plt
from scipy import misc

A=np.arange(30).reshape(5,6)   
C=np.zeros((5,6))
D=np.zeros((5+2,6+2))
        
D[1:5+1,1:6+1]=A

for i in range(1,5+1):
    for j in range(1,6+1):
        if i in[1,5] or j in[1,6]:
            if i==j or (i==1 and j==6) or (i==5 and j==1) or (i==5 and j==6):
                C[i-1, j-1]=np.sum(D[i-1:i+2, j-1:j+2])/4.0
            else:
                C[i-1, j-1]=np.sum(D[i-1:i+2, j-1:j+2])/6.0
        else:
            C[i-1, j-1]=np.sum(D[i-1:i+2, j-1:j+2 ])/9.0
                                              
print D,C
    
