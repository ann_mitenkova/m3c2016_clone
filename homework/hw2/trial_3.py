import numpy as np
import matplotlib.pyplot as plt
from scipy import misc
from sys import getsizeof
from scipy import linalg

if __name__ == '__main__':
    #Read and display image
    F = misc.imread('image.jpg')
    lx,ly,lz = F.shape


    #E=(F-F.min())*(1.0/(F.max()-F.min()))
    F2d=F[:,:,0]

def compress(F2d,C):
    """ Construct a compression of 2-d image matrix F2d which
    requires C times the memory needed for F2d
    """
    assert (0 < C < 1),"Parameter does not satisfy cropping criteria. The input value should be in the range [0,1)."
    size=(getsizeof(F2d))*C #what function to use???
    
    #print Fgray.nbytes, getsizeof(Fgray)
      
    X, D, Y = np.linalg.svd(F2d,full_matrices=False)
    Sigma=np.diag(D)
    a,b=Sigma.shape
    print a
    
    P=1
    Xcomp=X[:,0:P]
    Sigmacomp=Sigma[0:P,0:P]
    Ycomp=Y[0:P,:]
    Fcomp=np.dot(np.dot(Xcomp,Sigmacomp), Ycomp)
   
    
    Parray=np.zeros((1,a-1))
    for i in range(0,a-1):
        if Fcomp.size < size:
            Parray[0,P-1]=P
            P=P+1
            Xcomp=X[:,0:P]
            Sigmacomp=Sigma[0:P,0:P]
            Ycomp=Y[0:P,:]
            Fcomp=np.dot(np.dot(Xcomp,Sigmacomp), Ycomp)
        else:
            break
    Pvalues=P[0,0:P]
    print Pvalues