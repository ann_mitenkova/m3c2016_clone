"""Homework 2, part 2"""
"""Anna Mitenkova, CID:00927771"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def solveFlu(T,Nt,a,b0,b1,g,k,y0):
    """Obtains solution to Flu model from t=0 to t=T with
    initial conditions, y0, and model parameters, a,b0,b1,g,k.
    """
    #Solve the system of differential equations
    def RHS(y,t):
    
        S=y[0]
        E=y[1]
        C=y[2]
        R=y[3]
    
        b=b0+b1*(1+np.cos(2*np.pi*t))
        dS=k*(1-S)-b*C*S
        dE=b*C*S-(k+a)*E
        dC=a*E-(g+k)*C
        dR=g*C-k*R
        
        return [dS,dE,dC,dR]
    
    #Timepsan
    t=np.linspace(0,T,Nt)
    
    #Integrate ODE specified by RHS
    Y = odeint(RHS,y0,t)
    St=Y[:,0]
    Et=Y[:,1]
    Ct=Y[:,2]
    Rt=Y[:,3]
    
    #Initial test (plot of St+Et+Ct+Rt against time)
    """z=St+Et+Ct+Rt
    plt.figure()
    plt.plot(t,z)
    plt.xlabel('t')
    plt.ylabel('C+E+S+R')
    plt.title('User: Anna Mitenkova, function: solveFlu \n Changes in the sum of variables over time')
    plt.show()"""
    
    return St,Et,Ct,Rt,t
    
def displaySolutions(t,y):
    """Plot time series and phase plane for the numerical solution;
    input variable y contains S(t),E(t),C(t),R(t)"""
    
    #Plot the solutions
    plt.figure()
    plt.plot(t,y)
    plt.title('User: Anna Mitenkova, function: displaySolutions')
    plt.legend(loc='lower center')
    plt.show()
    
    #Labeling for E vs C (used when plotting E vs C). Put in red, so not to mess up the 
    #general output of the function.
    """plt.plot(t,y, label='Fraction exposed to disease vs fraction that is contagious')
    plt.xlabel('C(t)')
    plt.ylabel('E(t)')"""
    
    #Labeling for C vs t (used when plotting C vs t). Put in red, so not to mess up the 
    #general output of the function.
    """plt.plot(t,y, label='Changes in fraction that is contagious over time t')
    plt.xlabel('t')
    plt.ylabel('C(t)')"""
    
    
def linearFit(b0=5):
    """Estimate initial exponential growth rate for C(t) and 
    display actual and estimated growth vs. time
    """  
    #Set the parameters 
    a,b1,g,k = 1.0,0.2,0.2,0.1
    y0 = [0.997,0.001,0.001,0.001]
    T=10
    Nt=50000
    
    #Solve the system of equations
    St_neg,Et,Ct,Rt,t=solveFlu(T,Nt,a,b0,b1,g,k,y0)
    
    #Take the values less than 0.1
    C1=Ct[Ct<0.1] 
    t1=t[0:len(C1)]  
    
    #Find mu   
    mu=np.polyfit(t1,np.log(C1),1)   
    print mu
    
    #Create two plots
    plt.figure()
    plt.plot(t, np.log(Ct), label='Full solution C(t)') 
    plt.plot(t, mu[0]*t+mu[1], label='Estimated growth') 
    plt.title('User: Anna Mitenkova, function: linearFit \n Semilog plot') 
    plt.legend(loc='upper left')
    plt.xlabel('t')
    plt.ylabel('C(t)')  
    plt.show() 
    
    return mu 
               

def fluStats(t,y,i1,i2):
    """Compute mean and variance for data within the range, t[i1]<=t<=t[i2]
    provided the size Nt array, t and size Nt x 4 array y which contains 
    S(t),E(t),C(t),R(t) 
    """
    #Specify the criteria
    assert (t[i1]<=t[i2]),"Parameter does not satisfy the criteria: t[i1]<=t<=t[i2]"
    
    #Find mean and variance
    y2=y[i1:i2,:]
    mean=np.mean(y2,axis=0)
    variance=np.var(y2,axis=0)
    print mean,variance
    
    return mean,variance
         

if __name__ == '__main__':
    
    St,Et,Ct,Rt,t=solveFlu(5,50000,45.6,750.0,1000.0,73.0,1.0, [0.1,0.05,0.05,0.8]) 
 
    #Use displaySolutions function for values of t greater than 1  
    displaySolutions(t[t>1],Ct[len(t)-len(t[t>1]):len(t)])
    displaySolutions(Ct[len(t)-len(t[t>1]):len(t)],Et[len(t)-len(t[t>1]):len(t)])
    
    #Display mean and variance for the solutions
    fluStats(t,np.array([St,Et,Ct,Rt]).T,len(t)/2,len(t)-1)
