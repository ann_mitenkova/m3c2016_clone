import numpy as np
import matplotlib.pyplot as plt
from scipy import misc

if __name__ == '__main__':
    #Read and display image
    F = misc.imread('image.jpg')
    lx,ly,lz = F.shape

def smooth2(F):
    """ Apply a simple smoothing procedure to image matrix F
    """
    
    C=np.zeros((lx,ly,lz))
    D=np.zeros((lx+2,ly+2,lz))
    
    for k in range(0,3):
            
        D[1:lx+1, 1:ly+1, k]=F[:,:, k]

        for i in range(1,lx+1):
            for j in range(1,ly+1):
                if i in[1,lx] or j in[1,ly]:
                    if i==j or (i==1 and j==ly) or (i==lx and j==1) or (i==lx and j==ly) :
                        C[i-1, j-1,k]=np.sum(D[i-1:i+2, j-1:j+2,k])/4.0
                    else:
                        C[i-1, j-1,k]=np.sum(D[i-1:i+2, j-1:j+2,k])/6.0
                else:
                    C[i-1, j-1,k]=np.sum(D[i-1:i+2, j-1:j+2, k])/9.0
                                              
    A=(C-C.min())/(C.max()-C.min())                                                                                                                            
    plt.figure()
    plt.imshow(A[:,:,0])
    plt.show()
    
    plt.figure()
    plt.imshow(A[:,:,1])
    plt.show()
    
    plt.figure()
    plt.imshow(A[:,:,2])
    plt.show()
    
    plt.figure()
    plt.imshow(A)
    plt.show()
    
    return A.max(),C,A