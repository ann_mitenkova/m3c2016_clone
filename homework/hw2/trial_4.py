import numpy as np
import matplotlib.pyplot as plt
from scipy import misc
from sys import getsizeof
from scipy import linalg

if __name__ == '__main__':
    #Read and display image
    F = misc.imread('image.jpg')
    lx,ly,lz = F.shape

def compress(F2d,C):
    """ Construct a compression of 2-d image matrix F2d which
    requires C times the memory needed for F2d
    """
    assert (0 < C < 1),"Parameter does not satisfy cropping criteria. The input value should be in the range [0,1)."
    M=lx
    N=ly
    
    F2d=F[:,:,0]
    size=(M*N)*C 
      
    X, Psub, Y = np.linalg.svd(F2d,full_matrices=False)
    P=np.diag(Psub)
    
    #Assume we take first n values of P.Want C*M*N>=M*n+n*n+n*N. Need to solve n^2+n*(M+N)-C*M*N<=0, where n is an integer.
    
    D=(M+N)**2-4*(1-N*C*M)
    nroot=(-(M+N)+np.sqrt(D))/2 #Reject the other root as it is always negative
    n=int(nroot)
    
    Xcomp=X[:,0:n]
    P=P[0:n, 0:n]
    Ycomp=Y[0:n,:]
    
    print n, Xcomp, P, Ycomp
    
    
    
    """P=1
    Xcomp=X[:,0:P]
    Sigmacomp=Sigma[0:P,0:P]
    Ycomp=Y[0:P,:]
    Fcomp=np.dot(np.dot(Xcomp,Sigmacomp), Ycomp)
   
    while Fcomp.size < size:
        P=int(a/2)
    
    for i in range(0,a-1):
        if Fcomp.size < size:
            Parray[0,P-1]=P
            P=P+1
            Xcomp=X[:,0:P]
            Sigmacomp=Sigma[0:P,0:P]
            Ycomp=Y[0:P,:]
            Fcomp=np.dot(np.dot(Xcomp,Sigmacomp), Ycomp)
        else:
            break
    Parray=np.zeros((1,a-1))
            
    Pvalues=P[0,0:P]
    print Pvalues"""