! This code illustrates the basic structure of Fortran 90 code
! It reads an integer, N, from an input file and computes sin(x) for 
! x = 1,2,..., N
! To compile code:  gfortran -o example1.exe f90example1.f90
! Then, run executable: ./example1.exe

!1. Header:


!subroutine randseed

!    integer :: i2,clock,n
!    integer, dimension(:), allocatable :: seed
    
!    call random_seed(size=n)
!    allocate(seed(n))
    
!    call system_clock(count=clock)
    
!    seed = clock + 37 * (/ (i2 - 1, i2 = 1, n) /)
!    call random_seed(put = seed)
    
!    deallocate(seed)
    
!end subroutine randseed



program Trial2

    implicit none 
    integer :: i1,j1,l1,k1,N0,L,Nt,qmax,e1
    real(kind=8) :: sum1,sum2,u
    integer, dimension(:),allocatable :: qnet
    integer, dimension(:),allocatable :: nodes 
    real(kind=8), dimension(:),allocatable :: pnet, cpnet
    real(kind=8), dimension(:),allocatable :: unodes
    integer, dimension(:,:),allocatable :: enet,anet
    
    N0=5
    L=3
    Nt=3
    
    allocate(qnet(N0+Nt))
    allocate(nodes(L))
    allocate(pnet(N0+Nt))
    allocate(cpnet(N0+Nt))
    allocate(unodes(L))
    allocate(enet(2,N0+L*Nt))
    allocate(anet(N0+Nt,N0+Nt))
    
    sum1=0.d0
    e1=1
    
    !set up initial qnet
    do i1 = 1,N0 
        qnet(i1) = 2 
        sum1=sum1+qnet(i1)
    end do
    
    
    !set up initial enet
    do i1 = 1,N0
        enet(1,i1)=i1
        if (i1/=N0) then
            enet(2,i1)=i1+1
        else
            enet(2,i1)=1
        end if
    end do
    
    print *,'enet=', enet
    
    
    do k1 = 1,Nt 
        
        do i1 = 1,(N0-1+k1)              !loop from 1 to Nt-1
            pnet(i1)=qnet(i1)/sum1       !find pnet vector
            sum2=sum2+pnet(i1)
            cpnet(i1) = sum2             !find cumulative probability vector
        end do
        
        
        !call randseed
     
     
        do l1 = 1,L                      !generate L links
            
            call random_number(u)        !generate random number
            
            do j1 = 1,(N0-1+k1)               
                if (u <= cpnet(j1)) then !if u is in <= j1 entry, pick it as a random node
                    nodes(l1)=j1         !random nodes
                    !unodes(l1)=u        !random number (to check)
                    qnet(j1)=qnet(j1)+1  !increase degree by one of the chosen random nodes                    
                    enet(1,N0+e1)=N0+k1
                    enet(2,N0+e1)=j1
                    e1=e1+1
                    exit
                    
                end if
            end do
            
        end do
        
        qnet(N0+k1)=L                    !assign L values (degree) to a new node
        sum1=sum1+2*qnet(N0+k1)
        sum2=0.d0
        
        print *, 'cpnet=', cpnet
        print *,'unodes=',unodes
        print *,'nodes=',nodes
        print *, 'qnet=', qnet
        print *,'sum1=',sum1
        
    end do
    
    !print enet
    print *, 'enet= '
    do i1 = 1,N0+L*Nt
        write(*,*) enet(1,i1), enet(2,i1)
    end do
    

    !find max in qnet
    qmax=qnet(1)
    
    do i1 = 1,N0+Nt 
        if (qnet(i1) > qmax) then 
            qmax=qnet(i1)
        end if
    end do  
    
    print *, 'Maximum element in qnet is:   ', qmax 
    
    
    !find the adjacent matrix
    do i1 = 1,size(enet)/2 
        anet( enet(1,i1), enet(2,i1) )=anet( enet(1,i1), enet(2,i1) )+1
        anet( enet(2,i1), enet(1,i1) )=anet( enet(2,i1), enet(1,i1) )+1
    end do  
    
    !print anet
    print *, 'anet= '
    do i1 = 1,N0+Nt
        write(*,*) anet(i1,:)
    end do
    
!4. end program
end program Trial2
