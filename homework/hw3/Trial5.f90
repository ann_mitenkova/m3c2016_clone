program test_network
	use network
	implicit none
        integer :: qmax
        real(kind=8) :: c

!subroutine calls below should be modified as required and variables should be declared above as needed.
call generate(5,3,3,qmax)
call adjacency_matrix()
!call output_anet()
!call output_enet()
!call output_q()
call connectivity(c)
!call vary_connectivity()


end program test_network
