!Part 2 for M3C 2016 Homework 3
!by: Anna Mitenkova
!CID: 00927771


module network

!N0 - initial number of nodes
!Nt - number of timesteps
!L - number of links added at each timestep

integer, allocatable, dimension(:) :: qnet   !degree list
integer, allocatable, dimension(:,:) :: anet !adjacency matrix
integer, allocatable, dimension(:,:) :: enet !edge list
save
contains



!generate degree list and edge list
subroutine generate(N0,L,Nt,qmax)
    !Generate recursive matrix corresponding
    !to model parameters provided as input
	
    !all variables in code must be declared
    implicit none
    
    !define the type of the variables 
    integer, intent(in) :: N0,L,Nt
    integer, intent(out) :: qmax
    integer :: i1,j1,l1,k1,e1
    real(kind=8) :: sum1,sum2,u
    integer, dimension(:),allocatable :: nodes 
    real(kind=8), dimension(:),allocatable :: pnet, cpnet
    
    !allocate the dimensions 
    if(.not.allocated(qnet))allocate(qnet(N0+Nt))
    if(.not.allocated(anet))allocate(anet(N0+Nt,N0+Nt))
    if(.not.allocated(enet))allocate(enet(2,N0+L*Nt))
    allocate(nodes(L))
    allocate(pnet(N0+Nt))
    allocate(cpnet(N0+Nt))
    
    !set up initial values of sum1 and e1 to be zeros
    sum1=0
    e1=1
    
    !set up initial qnet
    qnet(:)=0
    do i1 = 1,N0 
        qnet(i1) = 2 
        sum1=sum1+qnet(i1)
    end do
    
    
    !set up initial enet
    enet(:,:)=0
    do i1 = 1,N0
        enet(1,i1)=i1
        if (i1/=N0) then
            enet(2,i1)=i1+1
        else
            enet(2,i1)=1
        end if
    end do
    
    !lopp from 1 to the value of Nt
    do k1 = 1,Nt 
        
        !loop from 1 to the value of Nt-1
        do i1 = 1,(N0-1+k1) 
         
            !set up the probability vector            
            pnet(i1)=qnet(i1)/sum1
              
            !calculate sum2 (sum of the probabilities up to the i1th entry inclusive)
            sum2=sum2+pnet(i1)
            
            !set up the cumulative probability vector
            cpnet(i1) = sum2             
        end do
     
        
        !generate L links
        do l1 = 1,L                      
            
            !generate random number
            call random_number(u)        
            
            !loop from 1 to value of N0-1+k1
            do j1 = 1,(N0-1+k1)   
            
                !if u is in less or equal than j1 entry, then pick it as a random node                   
                if (u <= cpnet(j1)) then 
                
                    !random nodes
                    nodes(l1)=j1
                    
                    !increase the degree by one for the chosen random node
                    qnet(j1)=qnet(j1)+1                     
                    enet(1,N0+e1)=N0+k1
                    enet(2,N0+e1)=j1
                    e1=e1+1
                    exit
                    
                end if
            end do
            
        end do
        
        !assign L values (degree) to a new node
        qnet(N0+k1)=L  
        
        !increase the number of degrees by 2*L                  
        sum1=sum1+2*qnet(N0+k1)
        
        !assign sum2 to be 0
        sum2=0.d0
        
    end do
    

    !find max in qnet
    qmax=qnet(1)
    
    do i1 = 1,N0+Nt 
        if (qnet(i1) > qmax) then 
            qmax=qnet(i1)
        end if
    end do  
    
    !print *, 'Maximum element in qnet is:   ', qmax - used when debugging
    !print*, qnet - used when debugging
    
    !deallocate all the array values but qnet, enet
    deallocate(nodes,pnet,cpnet)

end subroutine generate

    

subroutine adjacency_matrix()
	!Create adjacency matrix corresponding 
	!to pre-existing edge list 
	
	!all variables in code must be declared
	implicit none
	
	!define the type of the variables 
	integer :: i1
	
	!set up anet to be a matrix of zeros
	anet(:,:)=0
	
	!iterate on i1 from 1 to the value equal to the number of columns in enet (size(enet)/2)
	do i1 = 1, size(enet)/2
	
	    !assign the values to anet
            anet( enet(1,i1), enet(2,i1) )=anet( enet(1,i1), enet(2,i1) )+1
            anet( enet(2,i1), enet(1,i1) )=anet( enet(2,i1), enet(1,i1) )+1
        end do  


end subroutine adjacency_matrix

subroutine connectivity(c)
	!Compute connectivity of pre-computed network
	
	!all variables in code must be declared
	implicit none
	
	!define the type of the variables 
	integer :: i1,j1,dimsize, N, LDA, LWORK, INFO
	real(kind=8), intent(out) :: c
	real(kind=8), dimension(:,:), allocatable :: D, M, A
	real(kind=8), dimension(:), allocatable :: W, WORK
	
	!define dimsize to be the size of qnet
	dimsize=size(qnet)
	
	!define the dimensions of D and M
	allocate(D(dimsize,dimsize))
	allocate(M(dimsize,dimsize))
	
	!define matrix D to be the matrix of zeros
	D(:,:)=0
	
	!iterate on i1 from 1 to the value equal to the size of qnet
	do i1 = 1, dimsize
	   D(i1,i1)=qnet(i1)
	end do
	
	!calculate M
	M=D-anet
	 
	!define the values of N and LDA, weherN is the order of M (number of rows) and LDA- number of columns
	N=dimsize    
	LDA=dimsize  
	
	!allocate the dimensions 
	allocate(A(N,LDA))
	allocate(W(N))
	
	!define A
	A=M
	
	! Query the optimal size of the workspace.
        allocate (WORK(1))
        LWORK = -1
        call dsyev("N", "U", N, A, LDA, W, WORK, LWORK, INFO)
        LWORK = int(WORK(1))
        deallocate (WORK)
        allocate (WORK(LWORK))
	
	!Set N for JOBZ and U for UPLO, since we need to compute eigenvalues only
	call dsyev("N", "U", N, A, LDA, W, WORK, LWORK, INFO)
	if (INFO /= 0) stop 
	!print*, "INFO=", INFO - used when debugging
	!if INFO = -i, the i-th argument had an illegal value
	!if INFO = i, the algorithm failed to converge; i
	!off-diagonal elements of an intermediate tridiagonal
	!form did not converge to zero.  
	
	!assign the second smallest number of W to c
	c=W(2)
	
	!print c
	!print *, c - used when debugging
	
	!deallocate all the array values but qnet, enet, anet
	deallocate(D, M, A, W, WORK)
	

end subroutine connectivity


subroutine vary_connectivity(N0,Lmax,Nt,carray)
	!Compute connectivity for networks with L varying
	!between 1 and Lmax
	
	!From the created plot in python file hw3.py, we can see that the relationship 
	!between the algebraic connectivity and the values of L for vary_connectivity(8,6,4000)
	!is almost linear (the increase in the values of algebraic connectivity is almost
	!proportionate to the increase in the values of L)
	
	!It takes approximately 57 seconds to run the program for vary_connectivity(8,6,4000)
	
	!all variables in code must be declared
	implicit none
	
	!define the type of the variables 
	integer, intent(in) :: N0,Lmax,Nt
	integer :: qmax, L, clock_t1, clock_t2, clock_rate
	real(kind=8) :: c
	real(kind=8), dimension(Lmax),intent(out) :: carray
	
	!initialise the system clock
	call system_clock(clock_t1)
	
	!iterate on L from 1 to Lmax
	do L = 1,Lmax
	
	   !call all the previous subroutines
	   call generate(N0,L,Nt,qmax)
           call adjacency_matrix()
           call connectivity(c)
           
           !assign the value of c to the Lth entry of carray
           carray(L) = c
           
           !deallocate enet, qnet, anet
           deallocate(enet)
           deallocate(qnet)
           deallocate(anet)
           
        end do
        
        !print carray
        !print*, carray - used when debugging
        
        !stop systme clock to see how long it took for the program to run
        call system_clock(clock_t2,clock_rate)
        
        !print the elapsed time
        print *, 'elapsed wall time (seconds)= ',float(clock_t2-clock_t1)/float(clock_rate)
    
           

end subroutine vary_connectivity


end module network