! This code illustrates the basic structure of Fortran 90 code
! It reads an integer, N, from an input file and computes sin(x) for 
! x = 1,2,..., N
! To compile code:  gfortran -o example1.exe f90example1.f90
! Then, run executable: ./example1.exe

!1. Header:
program Trial1

    implicit none 
    integer :: i1,j1
    real :: sum,u
    integer, dimension(10) :: qnet, nodes
    real(kind=4), dimension(10) :: pnet, cpnet, unodes
    
    do i1 = 1,10 !loop from 1 to N0
        qnet(i1) = 2
        pnet(i1) = 2.d0/20.d0
        sum = sum+pnet(i1)
        cpnet(i1) = sum        
    end do
    
    do i1 = 1,10 !loop from 1 to N0
        call random_number(u)
        do j1 = 1,10   
            if (u <= cpnet(j1)) then
                nodes(i1)=j1
                exit
            end if
        end do
    end do
        

    print *,'qnet=',qnet
    print *,'pnet=',pnet
    print *,'cpnet=',cpnet
    print *,'nodes=',nodes

!4. end program
end program Trial1



	


	 
