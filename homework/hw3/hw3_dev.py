"""Part 1 for M3C 2016 Homework 3
   by: Anna Mitenkova
   CID: 00927771
"""

import numpy as np
import matplotlib.pyplot as plt
from n1 import network as net 


def visualize(e):    
    """Visualize a network represented by the edge list
    contained in e
    """
    #define e_unique to be a vector containing all the nodes (uniquelly) 
    e_unique=np.unique(e)
    
    #define e_col1 to be a vector of zeros of length N, where N is the number of rows in e
    e_col1=np.zeros((e.shape[0],2))
    
    #define e_col1 to be a vector of zeros of length N, where N is the number of rows in e
    e_col2=np.zeros((e.shape[0],2))
    
    #generate N random uniform variables, where N is the number of rows in e, and store them in vector x
    x=np.random.uniform(0,1,len(e_unique))
    
    #generate N random uniform variables, where N is the number of rows in e, and store them in vector y
    y=np.random.uniform(0,1,len(e_unique)) 
    
    #iterate on i in the range between 0 and e.shape[0], where e.shape[0] is the number of columns in e
    for i in range(0,e.shape[0]):
        e_col1[i,0]=x[e[i,0]-1]
        e_col1[i,1]=y[e[i,0]-1]
        e_col2[i,0]=x[e[i,1]-1]
        e_col2[i,1]=y[e[i,1]-1]

    #create a plot
    plt.figure()
    
    #plot the nodes, set the color of the nodes to be black
    plt.scatter(e_col1[:,0],e_col1[:,1], c='black', s=300)
    plt.scatter(e_col2[:,0],e_col2[:,1], c='black', s=300)
    
    #plot the links between the nodes, set the color of the links to be black
    for i in range(0,e.shape[0]):
        plt.plot([e_col1[i,0],e_col2[i,0]], [e_col1[i,1],e_col2[i,1]], 'k', linewidth=0.2)
     
    #add title and legend   
    plt.title('User: Anna Mitenkova, function: visualize')
    plt.legend(loc='lower right')
    
    #add grid
    plt.grid()
    
    #show the plot
    plt.show()
   
def degree(N0,L,Nt,display=False):
    """Compute and analyze degree distribution based on degree list, q,
    corresponding to a recursive network (with model parameters specified
    as input.
    
    For model parameters degree(5,2,4000,True), the output produced is saved as hw3.png
    The functional form of the distribution in the saved figured resemples very closely 
    the exponential decay pattern, and is approximated by:
    y=exp(x^3*(-1.22443757e-04)+x^2*(1.37998911e-02)+x*(-5.43303528e-01)+(-6.90588266e-01)
    """ 
    
    #call subroutine generate from fortran module network
    net.generate(N0,L,Nt) 
    
    #import qnet as an array called degrees
    degrees=np.array(net.qnet)

    #create vector d with values from 1 to max(degrees)+1
    d=np.arange(max(degrees))+1
    
    #find the total number of nodes
    totalnodes=len(degrees) 
    
    #create P1 matrix of zeros with dimensions 2 by length of d
    P1=np.zeros((2,len(d)))
    
    #create P2 vector of zeros with length equal to length of d
    P2=np.zeros((len(d)))
    
    #assign the first row of P1 to be equal to vector d
    P1[0,:]=d

    #iterate on i and j from 0 up to the values equal to the length of d and the length of degrees respectively
    for i in range(0,len(d)):
        for j in range(0,len(degrees)):
            #check if the value in the first row of P equals to the respective value in the vector degrees
            if P1[0,i]==degrees[j]:
                #if the values are equals, increase the value of entry by 1
                P1[1,i]=P1[1,i]+1
    
    #assign to P2 the values of P1 in the second row divided by the total number of nodes
    P2[:]=P1[1,:]/totalnodes
    
    #check the sum of all the entries of P2 adds up to 1
    print sum(P2)
    
    #set the fitrange to be half of the length of the vector degrees
    fitrange=int(len(degrees)/2)
    
    #set P2fit to be half of the vector P2
    P2fit=P2[0:fitrange]
    
    #set P2fit to be half of the vector d
    dfit=d[0:fitrange]
    
    #select the values that are not zero (none of them is negative), and assin to P2fit
    P2fit=P2fit[P2fit>0]
    
    #set dfit to contain the values of d, corresponding to the values of P2fit
    dfit=d[P2fit>0]
    
    #approximate the line fit (up to order 3)
    fit=np.polyfit(dfit,np.log(P2fit),3) 
    
    #print fit to see the values
    print fit
    
    #set up the range x
    x=np.arange(max(d))+1
    
    #check if the display is equal to True
    if display==True:
        
        #create a plot
        plt.figure()
        
        #set up a bar plot
        plt.bar(P1[0,:],P2[:], label='P(d) distribution', align='center')
        
        #plot the approximate fit to the distribution
        plt.plot(x,np.exp(x**(3)*fit[0]+x**(2)*fit[1]+x*fit[2]+fit[3]), color='red')
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: degree')
        plt.xlabel('d')
        plt.ylabel('P(d)')
        plt.legend()
        
        #plot the grid
        plt.grid()
        
        #show the plot
        plt.show() 
    
    #return P2 and d        
    return P2, d


if __name__ == "__main__":
    Degrees = True
    Vary_connectivity = False
    
    if Degrees == True:
        degree(5,2,4000,True)

    if Vary_connectivity == True:
        """From the created plot we can see that the relationship between the 
        algebraic connectivity and the values of L is almost linear (the increase 
        in the values of algebraic connectivity is almost proportionate to the 
        increase in the values of L)"""
        
        k=np.array(net.vary_connectivity(8,6,4000))
        x=np.arange(len(k))+1
        print k
    
        #create a plot
        plt.figure()
        plt.plot(x,k, label='Algebraic connectivity (c) vs L values')
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: vary_connectivity')
        plt.xlabel('L')
        plt.ylabel('c')
        plt.legend(loc='upper center')
        
        #plot the grid
        plt.grid()
        
        #show the plot
        plt.show() 
    
    
    
