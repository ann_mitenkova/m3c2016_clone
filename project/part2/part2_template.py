"""Project, part 2"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from n1 import network as net
from p1 import flunet as fn
from time import clock,time


def initialize(N0,L,Nt,pflag):
    """Generate network, and initial conditions
    If pflag is true, construct and return transport matrix
    """
    #call subroutine generate from fortran module network
    qmax,qnet,enet=net.generate(N0,L,Nt)
    
    #define N as the total number of nodes
    N=len(np.array(qnet))
    
    #define Incon as the vector of zeros of length 3*N
    Incon=np.zeros(3*N)
    
    #find index of the infected node (for initial condition this should correspond 
    #to the highest-degree node being infected
    index_infected=np.argmax(qnet)
    
    #assign values S=1, E=0, C=0 to Incon, in vector Incon the first N elements 
    #correspond to S with E and C following
    for i in range(0,3*N):
        Incon[0:N]=1
        Incon[N:2*N]=0
        Incon[2*N:3*N]=0
    
    #assign S=0.1, E=0.05, C=0.05 to the infected node
    Incon[index_infected]=0.1
    Incon[N+index_infected]=0.05
    Incon[2*N+index_infected]=0.05
    
    #adjust the index_infected to be the actual index of the infected node
    index_infected=index_infected+1
    
    #define P to be a matrix of zeros with dimensions N by N
    P=np.zeros((N,N))
    
    #if pflag is true, compute the transport matrix
    if pflag==True:
        
        #call adjacency_matrix to find anet
        anet=net.adjacency_matrix(N0+Nt,enet)
        
        #define P - transport matrix
        for j in range(0,N):
            P[:,j]=(qnet*anet[:,j]*1.0)/(np.dot(qnet,anet[:,j]))
    
        #return index of the node which has been infected-index_infected, the 
        #array Incon with initial conditions and the transport matrix P
        return Incon, index_infected, P
        
     
    #return index of the node which has been infected-index_infected, the 
    #array Incon with initial conditions
    return Incon, index_infected
    
    
def solveFluNet(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w,P,Incon,approach):
    """Simulate network flu model at Ntime equispaced times
    between 0 and T (inclusive). Add additional input variables
    as needed clearly commenting what they are and how they are  
    used
    """
    
    """Additional input variables:
            N0 - initial number of nodes
            L - number of links added at each timestep
            Nt - number of timesteps
            approach - the type of the approach used:
                       1-Python (def RHSnet)
                       2-Fortran (def RHSnetF)
                       3-Fortran+OpenMP (def RHSnetFomp)
            P - matrix P
    """
    #assign a value to numthreads
    numthreads=1
    
    #define N
    N=len(P[0,:])
    
    print P
      
    #define y0 - initial conditions
    y0=Incon
    
    #define St, Et, Ct as the matrix of zeros with dimensions Ntime and N   
    St=np.zeros((Ntime,N))
    Et=np.zeros((Ntime,N))
    Ct=np.zeros((Ntime,N))
    
    #if Python (1) approach is used:
    if approach==1:   
        
        def RHSnet(y,t,a,b0,b1,g,k,w):
            """RHS used by odeint to solve Flu model"""
        
            #define vectors S,E,C using the vector Inon with initial conditions
            S=y[0:N]
            E=y[N:2*N]
            C=y[2*N:3*N]
            
            #define dy
            dy=np.zeros((3*N))
        
            #define the system of equations
            b=b0+b1*(1.0+np.cos(2.0*np.pi*t))
            dS=k*(1.0-S)-b*C*S-w*S+w*np.dot(P,S)
            dE=b*C*S-(k+a)*E-w*E+w*np.dot(P,E)
            dC=a*E-(g+k)*C-w*C+w*np.dot(P,C)
         
            #assign the values to dy   
            dy[0:N]=dS
            dy[N:2*N]=dE
            dy[2*N:3*N]=dC
        
            #print and return dy
            
            return dy
         
        #Timepsan
        t=np.linspace(0,T,Ntime)   
          
        #Integrate ODE specified by RHS
        Y = odeint(RHSnet,y0,t,args=(a,b0,b1,g,k,w))
        
        
     
    #if Fortran (2) approach is used: 
    if approach==2:   
    
        def RHSnetF(y,t,a,b0,b1,g,k,w):
            """RHS used by odeint to solve Flu model"
            Calculations carried out by fn.rhs
            """
            dy=fn.rhs(y,P,t,a,b0,b1,g,k,w)
        
            return dy
        
        #Timepsan
        t=np.linspace(0,T,Ntime)  
        
        #Integrate ODE specified by RHS
        Y = odeint(RHSnetF,y0,t,args=(a,b0,b1,g,k,w))
        
    
    
    #if Fortran+OpenMP (3) approach is used: 
    if approach==3:                         
                                                            
        def RHSnetFomp(y,t,a,b0,b1,g,k,w):
            """RHS used by odeint to solve Flu model
            Calculations carried out by fn.rhs_omp
            """
            dy=fn.rhs_omp(y,P,t,a,b0,b1,g,k,w)
        
            return dy

        #Timepsan
        t=np.linspace(0,T,Ntime)  
        
        #Integrate ODE specified by RHS
        Y = odeint(RHSnetFomp,y0,t,args=(a,b0,b1,g,k,w))
    
    
    #assign the values to St, Et and Ct from Y-the solution vector
    St=Y[:,0:N]
    Et=Y[:,N:2*N]
    Ct=Y[:,2*N:3*N]
        
    #print t,St,Et and Ct
    print St
    print Et
    print Ct

    #return t,St,Et and Ct
    return t,St,Et,Ct


def analyze(N0,L,Nt,T,Ntime,a,b0,b1,g,k,threshold,warray,display=False):
    """analyze influence of omega on: 
    1. maximum contagious fraction across all nodes, Cmax
    2. time to maximum, Tmax
    3. maximum fraction of nodes with C > threshold, Nmax    
    Input: N0,L,Nt: recursive network parameters 
           T,Ntime,a,b0,b1,g,k: input for solveFluNet
           threshold: use to compute  Nmax
           warray: contains values of omega
    """
    
    #for each value in warray compute:
    
    Tmax=np.zeros((len(warray)))
    Cmax=np.zeros((len(warray)))
    C_threshold=np.zeros((Ntime))
    Nmax=np.zeros((len(warray)))
    
    #call initialize
    Incon,index_infected,P=initialize(N0,L,Nt,True)
    
    #define N
    N=len(P[:,0])
    
    for i in range(0,len(warray)):
        
        #simulate the model from solveFlunet
        t,St,Et,Ct=solveFluNet(N0,L,Nt,T,Ntime,a,b0,b1,g,k,warray[i],P,Incon,1)
        
        #define Cmax, the maximum contagious population observed on a single node
        Cmax[i]=np.amax(Ct)
        
        #define Tmax, the time at which Cmax occurs
        Tmax[i]=t[np.argmax(np.amax(Ct,0))]
        
        for j in range(0,Ntime):
            #values of Ct where Ct is greater than threshold
            Ct_2=Ct[j,:]
            C_threshold[j]=float(len(Ct_2[Ct_2>threshold]))/N
        
        #define Nmax, the maximum fraction of nodes with Ct greater than threshold 
        #at any time
        Nmax[i]=np.amax(C_threshold)
     
    print Cmax,Tmax, Nmax
    
    #print warray,Tmax,Cmax,Nmax       
                         
    #if display is true, create plots of of the three variables vs w
    if display==True:    
    
        #create a plot
        plt.figure()
        
        #plot Cmax vs w
        plt.plot(warray,Cmax, color='red', label='Cmax values' )
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: analyze')
        plt.xlabel('w')
        plt.ylabel('Cmax')
        plt.legend()
        
        #plot the grid
        plt.grid()
        
        
        
        #create a plot
        plt.figure()
        
        #plot Tmax vs w
        plt.plot(warray,Tmax, color='blue', label='Tmax values')
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: analyze')
        plt.xlabel('w')
        plt.ylabel('Tmax')
        plt.legend()
        
        #plot the grid
        plt.grid()
        
    
        
        #create a plot
        plt.figure()
        
        #plot Nmax vs w
        plt.plot(warray,Nmax, color='green', label='Nmax values')
        
        #add title, legend and axis labels
        plt.title('User: Anna Mitenkova, function: analyze')
        plt.xlabel('w')
        plt.ylabel('Nmax')
        plt.legend()
        
        #plot the grid
        plt.grid()
        
        #show the plot
        plt.show() 
        
    return Cmax,Tmax,Nmax
    

def visualize(enet,C,threshold):
    """Optional, not for assessment: Create figure showing nodes with C > threshold.
    Use crude network visualization algorithm from homework 3
    to display nodes. Contagious nodes should be red circles, all
    others should be black"""
    return None


def performance(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w):
    """function to analyze performance of python, fortran, and fortran+omp approaches
        Add input variables as needed, add comment clearly describing the functionality
        of this function including figures that are generated and trends shown in figures
        that you have submitted
    """
    #set up R value
    R=10
    
    #define x
    x=np.arange(R)+1
    
    #define elapsed_1 to be the vector of zeros of length R
    elapsed_1=np.zeros((R))
    
    #define elapsed_2 to be the vector of zeros of length R
    elapsed_2=np.zeros((R))
    
    #define elapsed_3 to be the vector of zeros of length R
    elapsed_3=np.zeros((R))
    
    for i in range(0,R):
        
        #calulate the start time
        start=time.clock()
        
        #call solveFlunet
        solveFluNet(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w, 1)
        
        #calculate the elapsed time
        elapsed=(time.clock() - start)
        elapsed_1[i] = elapsed



        #calulate the start time
        start=time.clock()
        
        #call solveFlunet
        solveFluNet(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w, 2)
        
        #calculate the elapsed time
        elapsed=(time.clock() - start)
        elapsed_2[i] = elapsed
        
        
        
        #calulate the start time
        start=time.clock()
        
        #call solveFlunet
        solveFluNet(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w, 3)
        
        #calculate the elapsed time
        elapsed=(time.clock() - start)
        elapsed_3[i] = elapsed
        
        
        
        
    #calculate the speedup
    speed_up_f=(1.0*elapsed_2)/elapsed_1 
    speed_up_fomp=(1.0*elapsed_3)/elapsed_1  
    
    #create a plot
    plt.figure()
        
    #plot Nmax vs w
    plt.plot(x,speed_up_f, color='green', label='Speed up with Fortran')
    
    #plot Nmax vs w
    plt.plot(x,speed_up_fomp, color='blue', label='Speed up with OpenMP')
        
    #add title, legend and axis labels
    plt.title('User: Anna Mitenkova, function: performance')
    plt.xlabel('R')
    plt.ylabel('Walltime speedup')
    plt.legend()
        
    #plot the grid
    plt.grid()
        
    #show the plot
    plt.show()   
    
    
    
if __name__ == '__main__':            
   a,b0,b1,g,k,w = 45.6,750.0,0.5,73.0,1.0,0.1
   T,Ntime=2,100
   N0,L,Nt=5,2,500
   
   
   Initialize=False
   Solveflunet=False
   Analyze=True
   Performance=False
   
   if Initialize==True:
       initialize(N0,L,Nt,True)
       
   if Solveflunet==True:
       Incon,index_infected,P=initialize(N0,L,Nt,True)
       solveFluNet(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w,P,Incon,1)
       
   if Analyze==True:
       analyze(N0,L,Nt,T,Ntime,a,b0,b1,g,k,0.1,[0,1e-2,1e-1,0.2,0.5,1],True)
    
   if Performance==True:
       performance(N0,L,Nt,T,Ntime,a,b0,b1,g,k,w)