module flunet
        use omp_lib
	implicit none
        !add variables as needed
	save
	contains


subroutine rhs(N,y,P,t,a,b0,b1,g,k,w,dy)
    implicit none
    !Return RHS of network flu model
    !input: 
    !n: total number of nodes
    !y: S,E,C - initial conditions
    !t: time
    !a,b0,b1,g,k,w: model parameters
    !output: dy, RHS
    integer, intent(in) :: N
    real(kind=8), dimension(N*3),intent(in) :: y
    real(kind=8), intent(in) :: t,a,b0,b1,g,k,w
    real(kind=8), dimension(N*3), intent(out) :: dy
    real(kind=8) :: Pi,b
    real(kind=8), dimension(N) :: dS,dE,dC,S,E,C
    real(kind=8), dimension(N,N), intent(in) :: P
    integer :: i1
    
    !define dS,dE,dC
    dS=0.d0
    dE=0.d0
    dC=0.d0
    
    !define number pi
    Pi=4.d0*datan(1.d0)
    
    !define S,E,C
    S(:)=y(1:N)
    E(:)=y(N:2*N)
    C(:)=y(2*N:3*N)

    !define b
    b=b0+b1*(1.0+cos(2.0*Pi*t))
    
    !define the system of equations
    do i1=1,N
        dS(i1)=k*(1-S(i1))-b*C(i1)*S(i1)-w*S(i1)+w*dot_product(P(i1,:),S)
        dE(i1)=b*C(i1)*S(i1)-(k+a)*E(i1)-w*E(i1)+w*dot_product(P(i1,:),E)
        dC(i1)=a*E(i1)-(g+k)*C(i1)-w*C(i1)+w*dot_product(P(i1,:),C)
    end do
   
    !define dy
    dy(1:N)=dS(:)
    dy(N:2*N)=dE(:)
    dy(2*N:3*N)=dC(:)
       
    
end subroutine rhs



subroutine rhs_omp(N,y,P,t,a,b0,b1,g,k,w,numthreads,dy)
    implicit none
    !Return RHS of network flu model, parallelized with OpenMP
    !input: 
    !n: total number of nodes
    !y: S,E,C
    !t: time
    !a,b0,b1,g,k,w: model parameters
    !numthreads: the parallel regions should use numthreads threads
    !output: dy, RHS
    integer, intent(in) :: N
    real(kind=8), dimension(N*3),intent(in) :: y
    real(kind=8), intent(in) :: t,a,b0,b1,g,k,w
    real(kind=8), dimension(N*3), intent(out) :: dy
    real(kind=8) :: Pi,b
    real(kind=8), dimension(N) :: dS,dE,dC,S,E,C
    real(kind=8), dimension(N,N), intent(in) :: P
    integer :: i1,numthreads,threadID

    !define dS,dE,dC
    dS=0.d0
    dE=0.d0
    dC=0.d0
    
    !define number pi
    Pi=4.d0*datan(1.d0)
    
    !define S,E,C
    S(:)=y(1:N)
    E(:)=y(N:2*N)
    C(:)=y(2*N:3*N)

    !define b
    b=b0+b1*(1.0+cos(2.0*Pi*t))
    
    call OMP_SET_NUM_THREADS(numThreads)
    
    !define the system of equations
    !use OMP tp parallelize the code
    !$OMP parallel do private(dS,dE,dC,threadID)
    do i1=1,N
        dS(i1)=k*(1-S(i1))-b*C(i1)*S(i1)-w*S(i1)+w*dot_product(P(i1,:),S)
        dE(i1)=b*C(i1)*S(i1)-(k+a)*E(i1)-w*E(i1)+w*dot_product(P(i1,:),E)
        dC(i1)=a*E(i1)-(g+k)*C(i1)-w*C(i1)+w*dot_product(P(i1,:),C)
        
        !assign omp_get_thread_num() to threadID
	threadID=omp_get_thread_num()
	   
    end do
    !$OMP end parallel do
   
   
    !define dy
    dy(1:N)=dS(:)
    dy(N:2*N)=dE(:)
    dy(2*N:3*N)=dC(:)

end subroutine rhs_omp



end module flunet