module sync
	implicit none
	complex(kind=8), parameter :: ii=cmplx(0.0,1.0) !ii = sqrt(-1)
	integer :: ntotal                                !total number of nodes
	real(kind=8) :: c                                !coupling coefficient
        real(kind=8), allocatable, dimension(:) :: w     !array of frequencies
        
	save
        contains


!---------------------------
subroutine rk4(t0,y0,dt,Nt,y,order)
    !4th order RK method
    
    !input:
    !t0: initial time
    !y0: initial condition (array)
    !dt: time step
    !nt: number of time steps
    
    !output:
    !y: solution at each time step (array)
    !order: order parameter
    
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
    integer, intent (in) :: Nt
    real(kind=8), dimension(size(y0),nt+1), intent(out) :: y
    real(kind=8), dimension(Nt), intent(out) :: order
    real(kind=8), dimension(size(y0)) :: f1, f2, f3, f4
    real(kind=8) :: t,halfdt,fac
    complex(kind=8) :: sume
    integer:: k,j1,N
    
    !define halfdt, fac
    halfdt = 0.5d0*dt
    fac = 1.d0/6.d0
    
    !define y
    y(:,:)=0.d0

    !initial condition
    y(:,1) = y0 
    
    !initial time
    t = t0 
    
    print*, t
    
    !define N
    N=size(y0)
  
    
    do k = 1, Nt !advance nt time steps

        f1 = dt*RHS(t, y(:,k))
        f2 = dt*RHS(t + halfdt, y(:,k) + 0.5d0*f1)
        f3 = dt*RHS(t + halfdt, y(:,k) + 0.5d0*f2)
        f4 = dt*RHS(t + dt, y(:,k) + f3)

        y(:,k+1) = y(:,k) + (f1 + 2*f2  + 2*f3 + f4)*fac

        t = t + dt
        
        !define sume to be zero
        sume=0.d0
        
        do j1=1,N
            sume=sume+exp(ii*y(j1,k+1))
        end do
        
        print*, 'sume', sume
        
        !calculate the order
        order(k)=(1.d0/k)*abs(sume)
           
    end do
        
        
end subroutine rk4



!---------------------------
function RHS(t,f)
    !RHS sync
    !f is the array of phases at time, t (theta)
    implicit none
    real(kind=8), intent(in) :: t
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)) :: RHS
    real(kind=8) :: sinsum
    integer :: i1,j1,N

    !define RHS
    RHS=0.d0
    
    !define N
    N=size(f)
    
    !allocate dimensions to w
    !allocate(w(N))
    
    !define the equation
    do i1=1,N
        
        !define sinsum to be 0
        sinsum=0.d0
        
        !calcuate the sum of sines
        do j1=1,N
        sinsum=sinsum+sin(f(i1)-f(j1))
        end do
        
        !calculate i-th df
        RHS(i1)=w(i1)-(c/N)*sinsum

    
    end do
 
 
end function RHS
!---------------------------


end module sync


