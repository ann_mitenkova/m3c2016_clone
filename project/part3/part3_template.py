# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from p3 import sync as sc  #assumes that fortran module sync has been compiled with f2py as p3.so



def oscillator(Nt,T,N,c,mu,sigma):
    """Simulate fully-coupled network of oscillators
    Compute simulation for Nt time steps between 0 and T (inclusive)
    input: N: number of oscillators
           c: coupling coefficient
           mu,sigma: distribution parameters for omega_i
    """

    #generate N natural frequencies ωi sampled from a random normal distribution
    #with mean 1 and standard deviation
    sc.w=np.random.normal(mu,sigma,N)
    
    #define c
    sc.c=c
    
    #generate the initial phases as N random numbers between 0 and 2π sampled 
    #from a unifom distributon
    y0=np.random.uniform(0,2*np.pi,N)
    
    #define dt (time step for rk4 algorithm)
    dt=(1.0*T)/Nt
    
    #define t0
    t0=0
    
    #define t
    t=np.linspace(0,T,Nt)
    
    #call rk4 from module sync
    y,order=sc.rk4(t0,y0,dt,Nt)
    
    #define omega
    omega=sc.w
    
    #return y, order
    return t,omega,y0,y,order
    


if __name__ == '__main__':
    N,c,mu,sigma = 101,10.0,1.0,0.1
    Nt,T = 500,100
    t,omega,theta0,theta,order = oscillator(Nt,T,N,c,mu,sigma)
