!Project part 1
module rwmodule
    use network

contains



subroutine rwnet(Ntime,Nm,X0,N0,L,Nt,isample,X,XM)
    !random walks on a recursive network
    !Input variables:
    !Ntime: number of time steps
    !Nm: number of walks
    !X0: initial node, (node with maximum degree if X0=0)
    !N0,L,Nt: recursive network parameters
    !Output: X, m random walks on network each with Ntime steps from initial node, X0
    !XM: fraction of walks at initial node, computed every isample timesteps
    !pnet: probabilities of walking from node X to an adjacent node Y
    
    implicit none
    integer, intent(in) :: Ntime,Nm,N0,L,Nt,X0,isample
    integer, dimension(Ntime+1,Nm), intent(out) :: X
    real(kind=8), allocatable, dimension(:) :: XM
    integer :: i1, j1, k1, qmax, totalx0, N
    integer, dimension(N0+Nt) :: qnet
    integer, dimension(N0+L*Nt,2) :: enet 
    integer, dimension((N0+L*Nt)*2) :: alist1
    integer, dimension(N0+Nt) :: alist2
    integer, dimension(N0+Nt,N0+Nt) :: anet
    integer, dimension(1) :: Maxlocation
    real(kind=8), allocatable, dimension(:) :: pnet, cnet, alist1net
    real(kind=8) :: u
    
    !allocate the dimensions to XM
    allocate(XM(int((Ntime+1)/isample)))
    
    !call subroutines from the network module
    call generate(N0,L,Nt,qmax,qnet,enet)
    call adjacency_list(qnet,enet,alist1,alist2)
    call adjacency_matrix(N0+Nt,enet,anet)
    
    
    !iterate over Nm random walks
    do i1=1,Nm
    
        !assign X0 as the first value of X
        X(1,i1)=X0
        
        !if X0=0, assign the first value of X to be the node with highest degree
        if (X0==0) then
            Maxlocation=maxloc(qnet)
            X(1,i1)=Maxlocation(1)
        end if
        
        do j1=2, (Ntime+1)
        
            !define N
            N=qnet(X(j1-1,i1))
            
            !allocate dimensions to cnet, pnet, alist1net
            allocate(pnet(N), cnet(N), alist1net(N))
            
            !define alist1net
            if (X(j1-1,i1)/=size(alist2)) then 
                alist1net=alist1(alist2(X(j1-1,i1)):(alist2(X(j1-1,i1)+1))-1)
            else 
                alist1net=alist1(alist2(X(j1-1,i1)):size(alist1))
            end if

                
            !convert degrees into probabilities
            do k1=1,N
                pnet(k1)=dble(anet(X(j1-1,i1),int(alist1net(k1))))/N
            end do

            
            !Convert pnet into cumulative probability, assign to cnet
            cnet(1) = pnet(1)
            cnet(N) = 1.d0
                
		do k1=2,N-1
			cnet(k1) = cnet(k1-1) + pnet(k1)
	        end do
	    
	    !set u to be zero
	    u=0
	    
	    !generate random number
            call random_number(u) 
            
            do k1=1,N
                !if u is in less or equal than k1 entry of cnet, then pick k1-th element from the (j1-1)th sublist of alist1                  
                if (u <= cnet(k1)) then 
                    X(j1,i1)=alist1net(k1)
                    exit
                end if
            end do
            
            !deallocate dimensions to cnet, pnet, alist1net
            deallocate(pnet, cnet, alist1net)
            
        end do
        
    end do
    
    !print matrix X
    print*, 'X='
    do i1=1,Nm
        print*, X(:,i1)
    end do
    
    !to compute XM every isample time steps, start by iterating over total number of time steps
    do i1=1,Ntime+1
    
        !check if the remainder of i1 when divided by isample is zero
        if (mod(i1, isample)==0) then 
        
            !assign initial value of sumx0 to be zero
            totalx0=0
            
            !iterate over total number of walks
            do j1=1,Nm
                
                !check if X(i1,j1)-th element is the initial node
                if (X(i1,j1)==X0) then
                    totalx0=totalx0+1   
                end if
                
            end do
            
        !compute the fraction of walkers that are at the initial node    
        XM(i1)=dble(totalx0)/Nm
        
        end if
    end do
    
    !print vector XM
    print*, 'XM='
    print*, XM

end subroutine rwnet


subroutine rwnet_omp(Ntime,Nm,X0,N0,L,Nt,isample,numthreads,X,XM)
    !parallelized version of rwnet, parallel regions should
    !use numthreads threads
	implicit none
    integer, intent(in) :: Ntime,Nm,N0,L,Nt,X0,isample,numthreads
    integer, dimension(Ntime+1,Nm), intent(out) :: X
    real(kind=8), allocatable, dimension(:) :: XM



end subroutine rwnet_omp


end module rwmodule